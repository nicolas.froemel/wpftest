﻿using System.ComponentModel;

namespace WpfTest1.Models
{
    class File
    {
        private string fileName;

        private string sha1Hash;

        private long size;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public string Sha1Hash
        {
            get { return sha1Hash; }
            set { sha1Hash = value; }
        }

        public long Size
        {
            get { return size; }
            set { size = value; }
        }

    }
}
