﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WpfTest1.ViewModels
{
    [ObservableObject]
    //Class has to be partial
    public partial class MainWindowViewModel
    {

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(ClickCommand))]
        private string? _test = "Hallo";

        //Attribute AllowConcurrentExecution allows clicking button multiple times when tasks already run
        //Added test
        [RelayCommand(CanExecute = nameof(CanClick), IncludeCancelCommand = true)]
        private async Task Click(CancellationToken token)
        {
            try
            {
                //Give cancelation token to child tasks
                await Task.Delay(5_000, token);
                //Test += " Moin";
            } catch(OperationCanceledException e)
            {
                MessageBox.Show("Cancelled.");
            }
        }
        private bool CanClick()
            => Test == "Hallo";


        // Communicate between ViewModels example
        // https://devblogs.microsoft.com/dotnet/announcing-the-dotnet-community-toolkit-800/
        public record UserLoggedInUserChangeMessage(string username);

        private void ComExample()
        {

            // Strong reference messenger ist faster, but keeps registered objects alive (no Garbage Collection)
            // -> Only use when proper unregistering is possible

            IMessenger messenger = WeakReferenceMessenger.Default;

            // Only (this) is also possible class has to have attribute [ObservableRecipient]
            // static function is mendatory when there are multiple instances of the view
            messenger.Register<UserLoggedInUserChangeMessage>(this, static (r,m) =>
            {
                // DO stuff
            });

            messenger.Send(new UserLoggedInUserChangeMessage("test"));

        }

        // https://learn.microsoft.com/de-de/dotnet/communitytoolkit/windows/getting-started

    }
}
