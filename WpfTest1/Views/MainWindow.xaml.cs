﻿using System.Windows;
using WpfTest1.ViewModels;

namespace WpfTest1.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //Easy usage because you dont have to cast DataContext to MainWindowViewModel
        private MainWindowViewModel ViewModel;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = ViewModel = new MainWindowViewModel();
        }
    }
}
